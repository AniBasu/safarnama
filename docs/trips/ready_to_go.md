# Ready to go?

<hr/>

## Before you step out

Turn on the location service of your phone, and share it (till you turn it off) with family and trusted friends.

## Things to carry

{% include 'common/things_to_carry_always.md' %}

!!! tip "Stay alert"

    Driving, with only yourself for company, can be soporific. Hence, music. And a towel and shopping bag, just so you have a reason to stop every 200 km, step out, and splash some water on your face at that roadside handpump.

{% include 'common/things_to_carry_trips.md' %}

!!! tip "Complete food"

    Always pull over if you see any stand selling coconut water.
	
	![tender coconuts](../images/IMG_1047.JPEG)

## Advice

### Plans :material-list-status:{ .maroon }

You can plan (you must!) but keep 50% room for known and unknown unknowns. For example, during Trip 2, on Day 1, I had a burst tyre despite getting the car checked at a company-authorised workshop just a few days earlier. Replacing the four tyres took two hours and 20,000 rupees.

### Car health

You can get your car checked at company-authorised workshops, but things will still go wrong. The quality of services at authorised workshops varies greatly. While they're fine for regular maintenance (for purposes of service history, should you sell your car later), it pays to locate and patronise a company-agnostic workshop (like Bosch's) in your city.

### Petrol and air

Shell stations and nitrogen air are not always available on highways. Additionally, many of the highway fuel stations sell only diesel. It isn't easy to locate company-owned-company-operated outlets on GoogleMaps. Try to fuel up after you reach the destination city.

### Washrooms

Largely unavailable (or unusable) if you're female. In an emergency, park the car and stash the key securely _on your person_, get in at a passenger seat and use a puke bag to do your business, use wet wipes liberally, dispose of the bag, sanitise your hands, and drive away.

### Mobile holders on dashboards

No; don't. It's a bad idea.

Direct sunlight + constant GPS (because, GoogleMaps + precise location + FindMyPhone) means even a car AC might not be enough cooling for a phone. Newer cars have navigation systems that are built right into the dashboard. But for older cars like mine, the front passenger seat is the best place for the phone.

### Footwear

Long drives need ankle support. Ditch the sandals and flipflops; get hiking or trekking shoes. Also, when you break for the night, shower your feet with TLC [^1] (warm water, moisturiser, massage) and keep them elevated for as long as possible. 

[^1]: TLC = tender loving care

### Headgear

Oftentimes, the sun is directly in your face. If you're as short as I am, the car visor isn't adequate protection for your eyes. To stop needing to adjust your cap's bill with every bend and curve on the road, get a wide-brimmed hat :material-redhat:{ .maroon }.

### Eyes :material-eye-refresh:{ .maroon }:material-eye-refresh:{ .maroon }

Everyone knows driving needs major brain CPU. No one mentions the dry eyes caused by unblinkingly staring at the road and minding the traffic. Yes, I do have polarised, anti-glare glasses. No, they don't quite help here. Every 3 hours, pause for 10 minutes and take those eye drops.

![add tears](../images/IMG_0637.jpeg)

### Food

Highway eateries are not really solo-woman-traveller-friendly. Carry your own food.

| Trail mix or similar  | Liquids |
| -----------------------------------| --- |
| ![trail mix](../images/IMG_0632.jpeg)| ![caffeinated drinks](../images/IMG_0737.jpeg) |

Also remember, highway restrooms for women are none too good. You'll have to figure out that fine balance between dehydration and the need to go looking for a washroom. What works for me is 3 - 4 gulps of that caffeinated drink every 90 or so minutes.

!!! tip "Dehydration"

    One of the first signs of dehydration is a low-grade headache. Pull over, drink, take a power nap.

| Dry fruits  | Puffed rice, flattened rice  |
| -----------------------------------| --- |
| ![dry fruits](../images/IMG_1120.jpeg)| ![puffed rice](../images/IMG_1166.jpeg) |

### Beauty

If the sun is constantly in front or to your right, you'll end up with suntanned arms.

Remedy? Lemon, yoghurt, gram flour, cucumber, honey. A combo of any two. (Truly a mix-and-match.)

![beauty treatment](../images/IMG_0645.jpeg)

(The cucumber is missing from the photo because I ate it before the procedure.)

Otherwise, consider using these scooter-riding gloves.

![driving gloves](../images/IMG_1148.jpeg)

